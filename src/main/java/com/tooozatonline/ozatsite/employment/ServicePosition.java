package com.tooozatonline.ozatsite.employment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ServicePosition {
    @Autowired
    private RepositoryPosition repositoryPosition;

    protected EntityPosition create(EntityPosition position) {
        return repositoryPosition.save(position);
    }
    protected EntityPosition findById(int id) {
        return repositoryPosition.findById(id);
    }
    protected EntityPosition read(int id) {
        return repositoryPosition.findById(id);
    }

    protected List<EntityPosition> readAll() {
        return repositoryPosition.findAll();
    }
    protected EntityPosition update(EntityPosition position) {
        EntityPosition temp = repositoryPosition.findById(position.getId());
        temp.setTitle(position.getTitle());
        return repositoryPosition.save(position);
    }
    protected void delete(int id) {
        repositoryPosition.deleteById(id);
    }
}