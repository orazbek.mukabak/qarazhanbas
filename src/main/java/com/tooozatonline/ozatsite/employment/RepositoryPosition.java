package com.tooozatonline.ozatsite.employment;

import org.springframework.data.jpa.repository.JpaRepository;

public interface RepositoryPosition extends JpaRepository<EntityPosition, Integer> {
    EntityPosition findById(int id);
}