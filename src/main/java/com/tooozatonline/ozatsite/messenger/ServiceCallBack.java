package com.tooozatonline.ozatsite.messenger;

import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.UpdatesListener;
import com.pengrad.telegrambot.request.SendMessage;
import com.pengrad.telegrambot.response.SendResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ServiceCallBack {
    @Autowired
    private RepositoryCallBack repositoryCallBack;
    protected EntityCallBack create(EntityCallBack callBack, String message) {
        try {
            TelegramBot bot = new TelegramBot("1229863724:AAHldA9LlWGV1Koq_p5uZ9EPTSHSWKlMM8I");
            bot.setUpdatesListener(updates -> {
                return UpdatesListener.CONFIRMED_UPDATES_ALL;
            });
            SendResponse response = bot.execute(new SendMessage(1166730813, message));
        } catch (Exception e) {}
        callBack.setMessage("");
        return repositoryCallBack.save(callBack);
    }
}