package com.tooozatonline.ozatsite.messenger;

import org.springframework.data.jpa.repository.JpaRepository;

public interface RepositoryCallBack extends JpaRepository<EntityCallBack, Integer> {
}