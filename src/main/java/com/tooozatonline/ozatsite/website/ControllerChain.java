package com.tooozatonline.ozatsite.website;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/chain")
public class ControllerChain {
    @Autowired
    private ServiceChain serviceChain;
    @PostMapping("/create")
    protected EntityChain create(@RequestBody EntityChain chain) {
        return serviceChain.create(chain);
    }
    @GetMapping("/read/{url}")
    protected Optional<EntityChain> read(@PathVariable String url) {
        return serviceChain.read(url);
    }
    @GetMapping("/read-all")
    protected List<EntityChain> readAll() {
        return serviceChain.readAll();
    }
}