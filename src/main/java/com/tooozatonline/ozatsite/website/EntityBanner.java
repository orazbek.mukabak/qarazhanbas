package com.tooozatonline.ozatsite.website;

import jakarta.persistence.*;
import jakarta.validation.constraints.Size;
import lombok.*;

@Entity
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "banner")
public class EntityBanner {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;
    @Size(min = 1, max = 127, message = "The url must have at least 1 and maximum 127 characters.")
    @Column(nullable = false, length = 127)
    private String url;
    @Size(max = 127, message = "The title must have maximum 127 characters.")
    @Column(length = 127)
    private String title;
    @Size(max = 255, message = "The description must have maximum 255 characters.")
    @Column(length = 255)
    private String description;
    @Size(max = 127, message = "The clickUrl must have maximum 127 characters.")
    @Column(length = 127)
    private String clickUrl;
    @Size(max = 127, message = "The clickName must have maximum 127 characters.")
    @Column(length = 127)
    private String clickName;
    @Size(max = 127, message = "The imageUrl must have maximum 127 characters.")
    @Column(nullable = false, length = 127)
    private String imageUrl;
    @Size(min = 1, max = 3, message = "The priority must be between 0 to 999 numbers.")
    @Column(nullable = false, length = 3)
    private int priority;
}