package com.tooozatonline.ozatsite.website;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ServiceBanner {
    @Autowired
    private RepositoryBanner repositoryBanner;

    protected EntityBanner create(EntityBanner Banner) {
        return repositoryBanner.save(Banner);
    }

    protected Optional<EntityBanner> read(int id) {
        return repositoryBanner.findById(id);
    }

    protected List<EntityBanner> readAll() {
        return repositoryBanner.findAll();
    }

    protected List<EntityBanner> readAllByUrl(String url) {
        return repositoryBanner.readAllByUrl(url);
    }
}