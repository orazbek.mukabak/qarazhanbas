package com.tooozatonline.ozatsite.website;

import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Size;
import lombok.*;

@Entity
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "chain")
public class EntityChain {
    @Id
    @Size(min = 1, max = 127, message = "The website url must have at least 1 and maximum 127 characters.")
    @Column(length = 127)
    private String url;
    @Column(nullable = false, unique = true)
    private int ownerId;
    @Size(min = 1, max = 127, message = "The website title must have at least 1 and maximum 127 characters.")
    @Column(nullable = false, length = 127)
    private String title;
    @Size(max = 255, message = "The website description must have at least 1 and maximum 255 characters.")
    @Column(nullable = false, length = 255)
    private String description;
    @Size(min = 1, max = 255, message = "The physical address must have at least 1 and maximum 255 characters.")
    @Column(nullable = false, length = 255)
    private String address;
    @Size(min = 1, max = 63, message = "The customer support staff name must at least 1 and maximum 63 characters.")
    @Column(nullable = false, length = 63)
    private String CRMName;
    @Size(min = 10, max = 15, message = "The website customer support mobile number must have at least 10 and maximum 15 characters.")
    @Column(nullable = false, length = 15)
    private String CRMMobile;
    @Size(min = 10, max = 15, message = "The website customer support whatsapp number must have at least 10 and maximum 15 characters.")
    @Column(nullable = false, length = 15)
    private String CRMWhatsapp;
    @Size(min = 1, max = 255, message = "The website customer support telegram account must have at least 1 and maximum 255 characters.")
    @Column(nullable = false, length = 255)
    private String CRMTelegram;
    @Size(min = 1, max = 255, message = "The website customer support instagram account must have at least 1 and maximum 255 characters.")
    @Column(nullable = false, length = 255)
    private String CRMInstagram;
    @Email(message = "Email must be well formed email.")
    @Size(min = 1, max = 127, message = "The website customer support email must have at least 1 and maximum 127 characters.")
    @Column(nullable = false, length = 127)
    private String CRMEmail;
    @Size(min = 10, max = 15, message = "The website technical support mobile number must have at least 10 and maximum 15 characters.")
    @Column(nullable = false, length = 15)
    private String techSupportMobileNumber;
}