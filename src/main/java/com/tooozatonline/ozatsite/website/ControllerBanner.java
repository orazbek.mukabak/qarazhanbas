package com.tooozatonline.ozatsite.website;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/banner")
public class ControllerBanner {
    @Autowired
    private ServiceBanner serviceBanner;

    @PostMapping("/create")
    protected EntityBanner create(@RequestBody EntityBanner banner) {
        return serviceBanner.create(banner);
    }

    @GetMapping("/read/{id}")
    protected Optional<EntityBanner> read(@PathVariable int id) {
        return serviceBanner.read(id);
    }

    @GetMapping("/read-all")
    protected List<EntityBanner> readAll() {
        return serviceBanner.readAll();
    }

    @GetMapping("/read-all-by-url/{url}")
    protected List<EntityBanner> readAllByUrl(@PathVariable String url) {
        return serviceBanner.readAllByUrl(url);
    }
}