package com.tooozatonline.ozatsite.userinfo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class ServiceUserInfo {
    @Autowired
    private RepositoryUserInfo repositoryUserInfo;
    @Autowired
    private PasswordEncoder passwordEncoder;
    protected EntityUserInfo create(EntityUserInfo userInfo) {
        userInfo.setPassword(passwordEncoder.encode(userInfo.getPassword()));
        userInfo.setRoles("ROLE_ADMIN");
        return repositoryUserInfo.save(userInfo);
    }
}