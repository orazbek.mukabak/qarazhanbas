package com.tooozatonline.ozatsite.userinfo;

import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Size;
import lombok.*;

@Entity
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "userinfo")
public class EntityUserInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Size(min = 10, max = 15, message = "The mobile number must have at least 10 and maximum 15 characters.")
    @Column(nullable = false, unique = true, length = 15)
    private String mobile;
    @Size(min = 5, max = 127, message = "The password must have at least 6 characters.")
    @Column(nullable = false, length = 127)
    private String password;
    @Email(message = "Email must be well formed email.")
    @Size(max = 127, message = "The email must have maximum 127 characters.")
    @Column(length = 127)
    private String email;
    @Size(max = 31, message = "The first name must have maximum 31 characters.")
    @Column(length = 31)
    private String firstName;
    @Size(max = 31, message = "The middle name must have maximum 31 characters.")
    @Column(length = 31)
    private String middleName;
    @Size(max = 31, message = "The last name must have maximum 31 characters.")
    @Column(length = 31)
    private String lastName;
    @Size(max = 31, message = "The roles must have maximum 255 characters.")
    @Column(length = 255)
    private String roles;
}