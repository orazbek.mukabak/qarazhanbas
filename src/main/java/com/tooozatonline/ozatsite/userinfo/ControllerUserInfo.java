package com.tooozatonline.ozatsite.userinfo;

import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
@RestController
@RequestMapping("/userinfo")
public class ControllerUserInfo {
    @Autowired
    private ServiceUserInfo serviceUserInfo;
    @PostMapping("/create")
    protected EntityUserInfo create(@RequestBody @Valid EntityUserInfo userInfo){
        return serviceUserInfo.create(userInfo);
    }
}