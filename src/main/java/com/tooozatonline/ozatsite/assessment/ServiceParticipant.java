package com.tooozatonline.ozatsite.assessment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ServiceParticipant {
    @Autowired
    private RepositoryParticipant repositoryParticipant;

    protected EntityParticipant create(EntityParticipant participant) {
        return repositoryParticipant.save(participant);
    }

    protected Optional<EntityParticipant> read(int id) {
        return repositoryParticipant.findById(id);
    }

    protected List<EntityParticipant> readAll() {
        return repositoryParticipant.findAll();
    }

    protected EntityParticipant update(EntityParticipant participant) {
        EntityParticipant temp = repositoryParticipant.findEntityParticipantById(participant.getId());
        temp.setFirstName(participant.getFirstName());
        temp.setLastName(participant.getLastName());
        return repositoryParticipant.save(participant);
    }

    protected void delete(int id) {
        repositoryParticipant.deleteById(id);
    }

    protected EntityParticipant finish(EntityParticipant participant) {
        EntityParticipant temp = repositoryParticipant.findEntityParticipantByVariantIdAndAuthor(participant.getVariantId(), participant.getAuthor());
        int totalCorrectQuestion = 0;
        String[] arrayAnswers = participant.getAnswers().split("OzK");
        String[] arrayCorrectAnswers = temp.getCorrectAnswers().split("OzK");
        for (int i = 0; i < arrayAnswers.length; i++) {
            if(arrayAnswers[i].equals(arrayCorrectAnswers[i])) {
                totalCorrectQuestion++;
            }
        }
        temp.setTotalCorrectQuestion(totalCorrectQuestion);
        temp.setAnswers(participant.getAnswers());
        temp.setStatus('f');
        return repositoryParticipant.save(temp);
    }

    protected EntityParticipant readByVariantIdAndAuthor(int variantId, int author) {
        return repositoryParticipant.findEntityParticipantByVariantIdAndAuthor(variantId, author);
    }
}