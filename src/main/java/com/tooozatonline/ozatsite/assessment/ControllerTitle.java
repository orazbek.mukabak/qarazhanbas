package com.tooozatonline.ozatsite.assessment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/title")
public class ControllerTitle {
    @Autowired
    private ServiceTitle serviceTitle;

    @PostMapping("/create")
    protected EntityTitle create(@RequestBody EntityTitle title, @RequestParam int subjectId) {
        return serviceTitle.create(title, subjectId);
    }

    @GetMapping("/read/{id}")
    protected Optional<EntityTitle> read(@PathVariable int id) {
        return serviceTitle.read(id);
    }

    @GetMapping("/read-all-by-subjectid/{id}")
    protected List<EntityTitle> readAllBySubjectId(@PathVariable int id) {
        return serviceTitle.readAllBySubjectId(id);
    }

    @PostMapping("/update")
    protected EntityTitle update(@RequestBody EntityTitle title) {
        return serviceTitle.update(title);
    }

    @DeleteMapping(value = "/delete/{id}")
    protected ResponseEntity<Integer> delete(@PathVariable int id) {
        serviceTitle.delete(id);
        return new ResponseEntity<>(id, HttpStatus.OK);
    }
}