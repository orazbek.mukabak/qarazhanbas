package com.tooozatonline.ozatsite.assessment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/participant")
public class ControllerParticipant {
    @Autowired
    private ServiceParticipant serviceParticipant;

    @PostMapping("/create")
    protected EntityParticipant create(@RequestBody EntityParticipant participant) {
        return serviceParticipant.create(participant);
    }

    @GetMapping("/read/{id}")
    protected Optional<EntityParticipant> read(@PathVariable int id) {
        return serviceParticipant.read(id);
    }
    @GetMapping("/read-by-variantid-and-author/{variantId}")
    protected EntityParticipant readByVariantIdAndAuthor(@PathVariable int variantId, @RequestParam("author") int author) {
        return serviceParticipant.readByVariantIdAndAuthor(variantId, author);
    }
    @GetMapping("/read-all")
    protected List<EntityParticipant> readAll() {
        return serviceParticipant.readAll();
    }

    @PostMapping("/update")
    protected EntityParticipant update(@RequestBody EntityParticipant participant) {
        return serviceParticipant.update(participant);
    }

    @DeleteMapping(value = "/delete/{id}")
    protected ResponseEntity<Integer> delete(@PathVariable int id) {
        serviceParticipant.delete(id);
        return new ResponseEntity<>(id, HttpStatus.OK);
    }

    @PostMapping("/finish")
    protected EntityParticipant finish(@RequestBody EntityParticipant participant) {
        return serviceParticipant.finish(participant);
    }
}