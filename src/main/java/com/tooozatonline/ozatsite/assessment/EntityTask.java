package com.tooozatonline.ozatsite.assessment;

import jakarta.persistence.*;
import jakarta.validation.constraints.Size;
import lombok.*;

import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "task")
public class EntityTask {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "question_id", nullable = false)
    private int questionId;
    @Column(name = "priority", nullable = false)
    private double priority;
    @Column(name = "correctanswer", nullable = false)
    private char correctAnswer;
    @Column(name = "fk_variant_id", nullable = false)
    private int variantId;
}