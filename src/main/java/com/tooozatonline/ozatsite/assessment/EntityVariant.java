package com.tooozatonline.ozatsite.assessment;

import jakarta.persistence.*;
import jakarta.validation.constraints.Size;
import lombok.*;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "variant")
//@JsonIgnoreProperties(value = {"questions"})
public class EntityVariant {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Size(min = 1, max = 127, message = "The title must have at least 1 and maximum 127 characters.")
    @Column(name = "name", nullable = false, length = 127)
    private String name;
    @Column(name = "timer", nullable = false)
    private boolean timer;
    @Column(name = "duration", nullable = false)
    private short duration;
    @Column(name = "updateddate")
    private LocalDateTime updatedDate;
    @Column(name = "fk_field_id", nullable = false)
    private int fieldId;
}