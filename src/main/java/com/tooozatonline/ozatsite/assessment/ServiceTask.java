package com.tooozatonline.ozatsite.assessment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class ServiceTask {
    @Autowired
    private RepositoryTask repositoryTask;
    @Autowired
    private RepositoryParticipant repositoryParticipant;

    protected EntityTask create(EntityTask task, int variantId) {
        task.setVariantId(variantId);
        return repositoryTask.save(task);
    }

    protected Optional<EntityTask> read(int id) {
        return repositoryTask.findById(id);
    }

    protected List<EntityTask> readAllByVariantId(int variantId) {
        List<EntityTask> tasks = repositoryTask.findByVariantIdOrderByPriorityAsc(variantId);
        if (tasks.size() > 0) {
            EntityParticipant participant = repositoryParticipant.findEntityParticipantByVariantIdAndAuthor(variantId, 1);
            if(participant == null) {
                createParticipant(tasks, variantId);
            } else {
                if(participant.getStatus() == 'c') {
                    repositoryParticipant.delete(participant);
                    createParticipant(tasks, variantId);
                }
            }
        }
        return tasks;
    }

    private EntityParticipant createParticipant(List<EntityTask> tasks, int variantId) {
        String questionIds = "";
        String taskIds = "";
        String correctAnswers = "";
        for(int i = 0; i < tasks.size(); i++) {
            questionIds += tasks.get(i).getQuestionId() + "OzK";
            taskIds += tasks.get(i).getId() + "OzK";
            correctAnswers += tasks.get(i).getCorrectAnswer() + "OzK";
        }
        questionIds = questionIds.substring(0,questionIds.length() - 3);
        taskIds = taskIds.substring(0,taskIds.length() - 3);
        correctAnswers = correctAnswers.substring(0,correctAnswers.length() - 3);
        EntityParticipant participant = new EntityParticipant(1,"ORAZBEK", "MUKABAK", variantId, tasks.size(), correctAnswers, questionIds, taskIds, LocalDateTime.now(), 'c');
        return repositoryParticipant.save(participant);
    }
}