package com.tooozatonline.ozatsite.assessment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/variant")
public class ControllerVariant {
    @Autowired
    private ServiceVariant serviceVariant;

    @PostMapping("/create")
    protected EntityVariant create(@RequestBody EntityVariant variant, @RequestParam int fieldId) {
        return serviceVariant.create(variant, fieldId);
    }

    @GetMapping("/read/{id}")
    protected Optional<EntityVariant> read(@PathVariable int id) {
        return serviceVariant.read(id);
    }

    @GetMapping("/read-all-by-fieldid/{id}")
    protected List<EntityVariant> readAllByFieldId(@PathVariable int id) {
        return serviceVariant.readAllByFieldId(id);
    }

    @PostMapping("/update")
    protected EntityVariant update(@RequestBody EntityVariant variant) {
        return serviceVariant.update(variant);
    }

    @DeleteMapping(value = "/delete/{id}")
    protected ResponseEntity<Integer> delete(@PathVariable int id) {
        serviceVariant.delete(id);
        return new ResponseEntity<>(id, HttpStatus.OK);
    }
}