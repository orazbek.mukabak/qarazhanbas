package com.tooozatonline.ozatsite.assessment;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface RepositorySubject extends JpaRepository<EntitySubject, Integer> {
    EntitySubject findEntitySubjectById(int id);
}