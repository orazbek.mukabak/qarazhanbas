package com.tooozatonline.ozatsite.assessment;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import jakarta.validation.constraints.Size;
import lombok.*;

@Entity
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "questionmcq")
public class EntityQuestionMCQ extends EntityQuestion {
    @Size(min = 1, max = 4095, message = "The question text must have at least 1 character")
    @Column(name = "text", nullable = false, length = 4095)
    private String text;
    @Size(max = 4095)
    @Column(name = "answera")
    private String answerA;
    @Size(max = 4095)
    @Column(name = "answerb")
    private String answerB;
    @Size(max = 4095)
    @Column(name = "answerc")
    private String answerC;
    @Size(max = 4095)
    @Column(name = "answerd")
    private String answerD;
    //@JsonIgnore
    @Column(name = "correctanswer", nullable = false)
    private char correctAnswer;
}