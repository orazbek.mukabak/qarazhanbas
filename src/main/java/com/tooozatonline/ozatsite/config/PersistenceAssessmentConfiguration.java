package com.tooozatonline.ozatsite.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;
import java.util.HashMap;

@Configuration
@PropertySource({"classpath:assessment-persistence-mysql.properties"})
@EnableJpaRepositories(
        basePackages = "com.tooozatonline.ozatsite.assessment",
        entityManagerFactoryRef = "assessmentEntityManager",
        transactionManagerRef = "assessmentTransactionManager"
)
public class PersistenceAssessmentConfiguration {
    @Autowired
    private Environment env;

    @Bean
    public LocalContainerEntityManagerFactoryBean assessmentEntityManager() {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(assessmentDataSource());
        em.setPackagesToScan(new String[]{"com.tooozatonline.ozatsite.assessment"});
        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        HashMap<String, Object> properties = new HashMap<>();
        properties.put("hibernate.hbm2ddl.auto", env.getProperty("assessment.hibernate.hbm2ddl.auto"));
        properties.put("hibernate.dialect", env.getProperty("assessment.hibernate.dialect"));
        properties.put("hibernate.show_sql", env.getProperty("assessment.hibernate.show_sql"));
        properties.put("hibernate.enable_lazy_load_no_trans", env.getProperty("assessment.hibernate.enable_lazy_load_no_trans"));
        em.setJpaPropertyMap(properties);
        return em;
    }

    @Bean
    public DataSource assessmentDataSource() {
        DriverManagerDataSource assessmentDataSource = new DriverManagerDataSource();
        assessmentDataSource.setDriverClassName(env.getProperty("assessment.jdbc.driver"));
        assessmentDataSource.setUrl(env.getProperty("assessment.jdbc.url"));
        assessmentDataSource.setUsername(env.getProperty("assessment.jdbc.user"));
        assessmentDataSource.setPassword(env.getProperty("assessment.jdbc.password"));
        return assessmentDataSource;
    }

    @Bean
    public PlatformTransactionManager assessmentTransactionManager() {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(assessmentEntityManager().getObject());
        return transactionManager;
    }
}