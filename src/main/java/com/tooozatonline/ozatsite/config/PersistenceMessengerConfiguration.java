package com.tooozatonline.ozatsite.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;
import java.util.HashMap;

@Configuration
@PropertySource({"classpath:messenger-persistence-mysql.properties"})
@EnableJpaRepositories(
        basePackages = "com.tooozatonline.ozatsite.messenger",
        entityManagerFactoryRef = "messengerEntityManager",
        transactionManagerRef = "messengerTransactionManager"
)
public class PersistenceMessengerConfiguration {
    @Autowired
    private Environment env;

    @Bean
    public LocalContainerEntityManagerFactoryBean messengerEntityManager() {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(messengerDataSource());
        em.setPackagesToScan(new String[]{"com.tooozatonline.ozatsite.messenger"});
        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        HashMap<String, Object> properties = new HashMap<>();
        properties.put("hibernate.hbm2ddl.auto", env.getProperty("messenger.hibernate.hbm2ddl.auto"));
        properties.put("hibernate.dialect", env.getProperty("messenger.hibernate.dialect"));
        properties.put("hibernate.show_sql", env.getProperty("messenger.hibernate.show_sql"));
        properties.put("hibernate.enable_lazy_load_no_trans", env.getProperty("messenger.hibernate.enable_lazy_load_no_trans"));
        em.setJpaPropertyMap(properties);
        return em;
    }

    @Bean
    public DataSource messengerDataSource() {
        DriverManagerDataSource messengerDataSource = new DriverManagerDataSource();
        messengerDataSource.setDriverClassName(env.getProperty("messenger.jdbc.driver"));
        messengerDataSource.setUrl(env.getProperty("messenger.jdbc.url"));
        messengerDataSource.setUsername(env.getProperty("messenger.jdbc.user"));
        messengerDataSource.setPassword(env.getProperty("messenger.jdbc.password"));
        return messengerDataSource;
    }

    @Bean
    public PlatformTransactionManager messengerTransactionManager() {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(messengerEntityManager().getObject());
        return transactionManager;
    }
}