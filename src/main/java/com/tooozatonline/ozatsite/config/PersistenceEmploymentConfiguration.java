package com.tooozatonline.ozatsite.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;
import java.util.HashMap;

@Configuration
@PropertySource({"classpath:employment-persistence-mysql.properties"})
@EnableJpaRepositories(
        basePackages = "com.tooozatonline.ozatsite.employment",
        entityManagerFactoryRef = "employmentEntityManager",
        transactionManagerRef = "employmentTransactionManager"
)
public class PersistenceEmploymentConfiguration {
    @Autowired
    private Environment env;

    @Bean
    public LocalContainerEntityManagerFactoryBean employmentEntityManager() {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(employmentDataSource());
        em.setPackagesToScan(new String[]{"com.tooozatonline.ozatsite.employment"});
        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        HashMap<String, Object> properties = new HashMap<>();
        properties.put("hibernate.hbm2ddl.auto", env.getProperty("employment.hibernate.hbm2ddl.auto"));
        properties.put("hibernate.dialect", env.getProperty("employment.hibernate.dialect"));
        properties.put("hibernate.show_sql", env.getProperty("employment.hibernate.show_sql"));
        properties.put("hibernate.enable_lazy_load_no_trans", env.getProperty("employment.hibernate.enable_lazy_load_no_trans"));
        em.setJpaPropertyMap(properties);
        return em;
    }

    @Bean
    public DataSource employmentDataSource() {
        DriverManagerDataSource employmentDataSource = new DriverManagerDataSource();
        employmentDataSource.setDriverClassName(env.getProperty("employment.jdbc.driver"));
        employmentDataSource.setUrl(env.getProperty("employment.jdbc.url"));
        employmentDataSource.setUsername(env.getProperty("employment.jdbc.user"));
        employmentDataSource.setPassword(env.getProperty("employment.jdbc.password"));
        return employmentDataSource;
    }

    @Bean
    public PlatformTransactionManager employmentTransactionManager() {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(employmentEntityManager().getObject());
        return transactionManager;
    }
}