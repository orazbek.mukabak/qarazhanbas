<%@ page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html><html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title></title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/footer.css" />
</head>
<body>
<div class='outer-wrapper-footer' id="outer_wrapper_footer">
    <div class='inner-wrapper-footer'>
        <div class='main-body-footer'>
            <div class='footer-items-group'>
                <div class='footer-items-group-header' id="title_footer"></div>
                <div class='footer-items-group-body'>
                    <div class='footer-item'>
                        <a href="${pageContext.request.contextPath}//">Басты бет</a>
                    </div>
                    <div class='footer-item'>
                        <a href="${pageContext.request.contextPath}/" id="about_us_footer">Біз туралы</a>
                    </div>
                    <div class='footer-item'>
                        <a href="${pageContext.request.contextPath}/contacts">Байланыс</a>
                    </div>
                    <sec:authorize access="!isAuthenticated()">
                        <div class='footer-item'>
                            <a href="${pageContext.request.contextPath}/signin?u=null&p=null">Кіру</a>
                        </div>
                    </sec:authorize>
                    <sec:authorize access="isAuthenticated()">
                        <div class='footer-item'>
                            <a href="${pageContext.request.contextPath}/logout">Шығу</a>
                        </div>
                    </sec:authorize>
                </div>
            </div>

            <div class='footer-items-group'>
                <div class='footer-items-group-header'>
                    Байланыс
                </div>
                <div class='footer-items-group-body'>
                    <div class='footer-item' id="tech_support_footer"></div>
                    <div class='footer-item' id="address_footer">
                    </div>
                    <div class='footer-item' id="email_footer"></div>
                </div>
            </div>

            <div class='footer-items-group'>
                <div class='footer-items-group-header'>
                    Желіде
                </div>
                <div class='footer-items-group-body'>
                    <div class='footer-item footer-item-sn'>
                        <a href="" id="whatsapp_footer"><img src="${pageContext.request.contextPath}/images/website/whatsapp.svg"></a>
                        <a href="" id="instagram_footer"><img src="${pageContext.request.contextPath}/images/website/instagram.svg"></a>
                    </div>
                </div>
            </div>
        </div>
        <div class='bottom-part-footer' id="copyright_footer"></div>
    </div>
</div>
<script>
    document.getElementById("outer_wrapper_footer").style = "background: url('" + getEnvCurrentUploadPath() + "%2Fimages%2Ffooters%2F" + getEnvCurrentWebsiteUrl() + ".svg?alt=media')center center; background-size: auto 100%; background-color: #f1f1f1;";
    document.getElementById("title_footer").innerHTML = getEnvCurrentWebsiteTitle();
    document.getElementById("tech_support_footer").innerHTML = getEnvCurrentWebsiteCRMName();
    document.getElementById("address_footer").innerHTML = getEnvCurrentWebsiteAddress();
    document.getElementById("about_us_footer").href = "${pageContext.request.contextPath}/about-us/" + getEnvCurrentWebsiteUrl();
    document.getElementById("offer_footer").href = "${pageContext.request.contextPath}/offer/" + getEnvCurrentWebsiteUrl();
    document.getElementById("privacy_footer").href = "${pageContext.request.contextPath}/privacy/" + getEnvCurrentWebsiteUrl();
    document.getElementById("bank_statement_footer").href = "${pageContext.request.contextPath}/bank-statement/" + getEnvCurrentWebsiteUrl();
    document.getElementById("email_footer").innerHTML = getEnvCurrentWebsiteCRMEmail();
    document.getElementById("whatsapp_footer").href = "https://wa.me/+7" + getEnvCurrentWebsiteCRMWhatsapp() + "?text=Сәлеметсіз бе, " + getEnvCurrentWebsiteTitle() + " сайты бойынша хабарласып тұр едім.";
    document.getElementById("instagram_footer").href = "https://www.instagram.com/" + getEnvCurrentWebsiteCRMInstagram() + "/";
    document.getElementById("copyright_footer").innerHTML = "© " + getEnvCurrentWebsiteTitle() + ", " + new Date().getFullYear() + " Барлық құқықтар қорғалған";
</script>
</body>
</html>