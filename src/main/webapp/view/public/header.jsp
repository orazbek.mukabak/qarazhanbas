<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<sec:authorize access="isAuthenticated()">
    <sec:authentication property="principal.username" var="username" />
</sec:authorize>

<!DOCTYPE html><html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title></title>
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/images/website/favicon.ico" type="image/x-icon" id="favicon">
    <script src="${pageContext.request.contextPath}/js/init.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css" integrity="sha512-UJfAaOlIRtdR+0P6C3KUoTDAxVTuy3lnSXLyLKlHYJlcSU8Juge/mjeaxDNMlw9LgeIotgz5FP8eUQPhX1q10A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/main.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/custom-sweet-alert.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/popup.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/header.css" />
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons+Outlined" rel="stylesheet">
</head>
<body>
<nav>
    <div class="nav-wrapper">
        <a href="${pageContext.request.contextPath}/" id="logo_header" class="brand-logo"></a>
        <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons-outlined">menu</i></a>
        <ul class="right hide-on-med-and-down">
            <li><a href="${pageContext.request.contextPath}/"><spring:message code="header.home" /></a></li>
            <sec:authorize access="!isAuthenticated()">
                <li><a class="signin-header" href="${pageContext.request.contextPath}/?#popupSignup"><spring:message code="header.register" /></a></li>
                <li><a class="signin-header" href="${pageContext.request.contextPath}/?#popupSignin"><spring:message code="header.login" /></a></li>
            </sec:authorize>
            <sec:authorize access="isAuthenticated()">
                <li><a href="javascript: displayTechSupportHeader();"><span class="material-icons-outlined">privacy_tip</span><spring:message code="header.tech.support" /></a></li>
                <li><a href="logout"><spring:message code="header.logout" /></a></li>
            </sec:authorize>
            <li>
                <select id="locales">
                    <option value="kz">Қазақша</option>
                    <option value="en">English</option>
                    <option value="ru">Русский</option>
                </select>
            </li>
            <li><a class="expand" href="${pageContext.request.contextPath}/?#popupSetting"><span class="material-icons-outlined">settings</span>Сайтты дамыту</a></li>
        </ul>
    </div>
</nav>

<ul id="mobile-demo" class="sidenav">
    <sec:authorize access="isAuthenticated()">
        <li><div class="user-view">
            <div class="background">
                <img src="${pageContext.request.contextPath}/images/website/bg-menu.svg">
            </div>
            <a href="${pageContext.request.contextPath}/profile/read-my-profile-private"><span class="white-text name" id="name_mobile_header"></span></a>
        </div></li>
        <li><a class="waves-effect" href="${pageContext.request.contextPath}/course/read-my-courses-private"><i class="material-icons-outlined">play_lesson</i><spring:message code="header.my.courses" /></a></li>
        <li><div class="divider"></div></li>
        <li><a class="waves-effect" href="${pageContext.request.contextPath}/profile/read-my-profile-private"><i class="material-icons-outlined">manage_accounts</i><spring:message code="header.my.profile" /></a></li>
        <li><div class="divider"></div></li>
        <li><a class="waves-effect" href="${pageContext.request.contextPath}/logout"><i class="material-icons-outlined">logout</i><spring:message code="header.logout" /></a></li>
    </sec:authorize>
    <sec:authorize access="!isAuthenticated()">
        <li><a class="subheader"><spring:message code="header.message" /></a></li>
        <li><div class="divider"></div></li>
        <li><a class="waves-effect" href="${pageContext.request.contextPath}/"><i class="material-icons-outlined left">account_balance</i><spring:message code="header.home" /></a></li>
        <li><div class="divider"></div></li>
        <li><a class="waves-effect" href="${pageContext.request.contextPath}/?#popupSignup"><i class="material-icons-outlined left">person_add</i><spring:message code="header.register"/></a></li>
        <li><a class="waves-effect" href="${pageContext.request.contextPath}/?#popupSignin"><i class="material-icons-outlined left">login</i><spring:message code="header.login" /></a></li>
    </sec:authorize>
    <li><div class="divider"></div></li>
    <li>
        <select id="locales_mobile" >
            <option value="kz">Қазақша</option>
            <option value="en">English</option>
            <option value="ru">Русский</option>
        </select>
    </li>
    <li><div class="divider"></div></li>
</ul>
<script src="${pageContext.request.contextPath}/js/header.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.11.6/umd/popper.min.js" integrity="sha512-6UofPqm0QupIL0kzS/UIzekR73/luZdC6i/kXDbWnLOJoqwklBK6519iUnShaYceJ0y4FaiPtX/hRnV/X/xlUQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.3/jquery.min.js" integrity="sha512-STof4xm1wgkfm7heWqFJVn58Hm3EtS31XFaagaa8VMReCXAkQnJZ+jEy8PCC/iT18dFy95WcExNHFTqLyp72eQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js" integrity="sha512-NiWqa2rceHnN3Z5j6mSAvbwwg3tiwVNxiAQaaSMSXnRRDh5C2mk/+sKQRw8qjV1vN4nf8iK2a0b048PnHbyx+Q==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.7.2/sweetalert2.min.js" integrity="sha512-jSNGQoIZ0qago6DK45skZbDI1JC8bmANSwItgDnMXiAnJm0Lq6QB4yXY8QPKqS68iR3ngZi0pM5+wZvg1kCCKQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.7.2/sweetalert2.all.min.js" integrity="sha512-SK3zv9THqmpMz5vgYjzMUplj1kKAkRC6j+xIC0XPcrGKntTmxhYQzgsysgwDq+WKG0CARR1YQKoqGsNlaNMvKg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<!--script src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/3.2.2/es5/startup.js" integrity="sha512-MX1gf/vgby10l/kdwswRVeldCBh5oHrhKOUNC5TjHh9wJui1UO62UxGT73T6nYo7ViCWg/Qo4clj8JJvgic7bg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script-->
<%@ include file="signup.jsp" %>
<%@ include file="signin.jsp" %>
<%@ include file="expand.jsp" %>
<script>
    // Identify local default language
    $(document).ready(function() {
        var selItem = localStorage.getItem("locales");
        $('#locales').val(selItem ? selItem : 'en');
        $('#locales').change(function() {
            var selectedOption = $('#locales').val();
            if (selectedOption) {
                window.location.replace('?lang=' + selectedOption);
                localStorage.setItem("locales", selectedOption);
            }
        });

        var selItem2 = localStorage.getItem("locales");
        $('#locales_mobile').val(selItem2 ? selItem2 : 'en');
        $('#locales_mobile').change(function() {
            var selectedOption2 = $('#locales_mobile').val();
            if (selectedOption2) {
                window.location.replace('?lang=' + selectedOption2);
                localStorage.setItem("locales", selectedOption2);
            }
        });
    });

    //   document.getElementById("profile_img_header").src = "images/website/back-avatar.png";

    <sec:authorize access="isAuthenticated()">
    function readProfile(username) {
        fetch("${pageContext.request.contextPath}/profile/read-profile-by-username-list/" + username)
            .then(response => response.json())
            .then(data => {
                document.getElementById("profile_img_header").src = getEnvCurrentFirebasePath() + "images%2Fprofiles%2F" + data[0].innerId + ".jpg?alt=media";
                document.getElementById("name_mobile_header").innerHTML = data[0].lastName + " " + data[0].firstName;
                setEnvCurrentUserEmail(data[0].email);
                setEnvCurrentUserInnerId(data[0].InnerId);
            });
    }
    //readProfile("${username}");

    function sessionHeader() {
        var httpRequest = new XMLHttpRequest();
        httpRequest.open('GET', "${pageContext.request.contextPath}/session");
        httpRequest.send(null);
    }
    //setInterval(sessionHeader, 60000);
    </sec:authorize>

    function initHeader() {
        document.getElementById('logo_header').style = "background: url('${pageContext.request.contextPath}/images/website/logo.svg') no-repeat center center; background-size: contain; height: 45px; width: 135px; min-width: 135px; margin: 10px 0;";
        document.title = getEnvCurrentWebsiteTitle();
        document.getElementById('favicon').href = "${pageContext.request.contextPath}/images/website/favicon.ico";
    }
    initHeader();
</script>
</body>
</html>