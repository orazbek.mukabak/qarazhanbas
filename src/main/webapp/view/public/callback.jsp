<%@ page contentType="text/html" pageEncoding="UTF-8" %>

<!DOCTYPE html><html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/callback.css" />
    <title></title>
</head>
<body>
    <div class='outer-wrapper-call'>
        <button class="open-button" onclick="openForm()"><span class="material-icons-outlined">headset_mic</span><spring:message code="index.call" /></button>
    </div>
    <script>
    function openForm() {
        Swal.fire({
            html:
                "Қоңырау шалу үшін жоғарыда көрсетілген нөмірді басыңыз. Ватсап желісімен хабарласу үшін <b><a href='https://wa.me/+7" + getEnvCurrentWebsiteCRMWhatsapp() + "?text=Сәлеметсіз бе, Qarajanbas акционерлік қоғамының емтихан тапсыру сайты бойынша хабарласып тұр едім.'>осында басыңыз.</a></b> Немесе төмендегі бос орынға телефон нөміріңізді жазып <b>Маған хабарлас</b> батырмасын басыңыз. Біздің мамандар сізге 30 минут ішінде міндетті түрде хабарласатын болады.",
            input: "text",
            inputPlaceholder: "Телефон нөміріңіз...",
            title: "<a href='tel:+7" + getEnvCurrentWebsiteCRMMobile() + "'><span class='material-icons-outlined'>headset_mic</span>+7" + getEnvCurrentWebsiteCRMMobile() + "</a>",
            imageUrl: "${pageContext.request.contextPath}/images/website/call.jpg",
            imageWidth: 500,
            inputAttributes: {
                autocapitalize: "off"
            },
            confirmButtonText: "Маған хабарлас",
            confirmButtonColor: "#222c31",
            showLoaderOnConfirm: true,
            preConfirm: (login) => {
                fetch('${pageContext.request.contextPath}/callback/create', {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({ "mobile": login, "message": "message here" })
                })
                    .then(response => response.json())
                    .then(response => {
                        Swal.fire({
                            title: 'Біз сізге хабарласамыз',
                            text: 'Біздің мамандарымыз сізге  30 минут ішінде міндетті түрде хабарласатын болады.',
                            confirmButtonText: 'ОК',
                            confirmButtonColor: "#222c31"
                        });
                    })
            }
        })
    }
    </script>
</body>
</html>