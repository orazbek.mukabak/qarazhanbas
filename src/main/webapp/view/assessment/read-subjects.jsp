<%@ page contentType="text/html" pageEncoding="UTF-8" %>

<!DOCTYPE html><html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/assessment/read-subjects.css"/>
</head>
<body>
<div class="read-subjects-wrapper">
    <div class="left-wrapper-read-subjects">
        <div class="collapsible-wrapper">
            <ul class="collapsible" id="subject_list"></ul>
        </div>
        <div class='new-subject'>
            <input id="new_subject_name" type="text" placeholder="Жаңа пән атауы" minlength="1" maxlength="127">
            <button onclick="newSubjectReadSubjects();"><span class="material-icons-outlined">add</span></button>
        </div>
    </div>
    <div class="right-wrapper-read-subjects unselectable" id="right_wrapper_read_subjects">
        <div class="placeholder-wrapper-read-subjects">
            Бұл орынға аздап мәтін/сурет/видео салып қойсақ болады.
        </div>
    </div>
</div>
<script>
    function printReadSubjects(div) {
        let myWindow = window.open('T', 'PRINT', 'height=650,width=900,top=100,left=150');
        myWindow.document.write(`<html><head><title>${title}</title>`);
        myWindow.document.write('</head><body >');
        myWindow.document.write(document.getElementById(div).innerHTML);
        myWindow.document.write('</body></html>');
        myWindow.document.close();
        myWindow.focus();
        myWindow.print();
        myWindow.close();
        return true;
    }

    function updateSubjectReadSubjects(id, oldName) {
        Swal.fire({
            title: 'Пәннің атауын өңдеу',
            input: 'text',
            inputValue: oldName,
            inputAttributes: {autocapitalize: 'off'},
            showCancelButton: true,
            confirmButtonColor: '#222c31',
            cancelButtonColor: '#222c31',
            confirmButtonText: 'Иә',
            cancelButtonText: 'Жоқ',
            preConfirm: (name) => {

                fetch('${pageContext.request.contextPath}/subject/update', {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({"id": id, "name": name})
                })
                    .then(response => {
                        M.toast({
                            html: '<span><spring:message code="signup.successful.message" /></span>',
                            displayLength: 2000,
                            classes: 'green',
                            completeCallback: () => {
                                window.location.href = "${pageContext.request.contextPath}/subject/edit";
                            }
                        });
                    })

            }

        });
    }

    function deleteSubjectReadSubjects(id, name) {
        Swal.fire({
            title: '"' + name + '" пәндік атауын өшіресіз бе?',
            text: "Осы пәндік атау ішіндегі барлық тақырыптар мен сұрақтар түбегейлі жойылатын болады!",
            showCancelButton: true,
            confirmButtonColor: '#222c31',
            cancelButtonColor: '#222c31',
            confirmButtonText: 'Иә',
            cancelButtonText: 'Жоқ'
        }).then((result) => {
            if (result.value) {
                fetch('${pageContext.request.contextPath}/subject/delete/' + id, {
                    method: 'DELETE',
                })
                    .then(res => res.text())
                    .then(res => {
                            M.toast({
                                html: '<span><spring:message code="signup.successful.message" /></span>',
                                displayLength: 2000,
                                classes: 'green',
                                completeCallback: () => {
                                    window.location.href = "${pageContext.request.contextPath}/subject/edit";
                                }
                            });
                        }
                    )
            }
        });
    }

    function postSubjectReadSubjects(subject) {
        fetch('${pageContext.request.contextPath}/subject/create', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(subject)
        })
            .then(response => response.json())
            .then(response => {
                if (response) {
                    if (response.id && response.id > 0) {
                        M.toast({
                            html: '<span><spring:message code="signup.successful.message" /></span>',
                            displayLength: 2000,
                            classes: 'green',
                            completeCallback: () => {
                                window.location.href = "${pageContext.request.contextPath}/subject/edit";
                            }
                        });
                    } else {
                        M.toast({
                            html: '<span><spring:message code="signup.tech.support.message" /></span>',
                            classes: 'red accent-2'
                        });
                    }
                } else {
                    M.toast({
                        html: '<span><spring:message code="signup.tech.support.message" /></span>',
                        classes: 'red accent-2'
                    });
                }
            })
    }

    function newSubjectReadSubjects() {
        let name = document.getElementById("new_subject_name").value;
        if (name.length > 0 && name.length < 127) {
            postSubjectReadSubjects({"name": name});
        } else {
            M.toast({
                html: '<span><spring:message code="signup.invalid.phone.message" /></span>',
                classes: 'red accent-2'
            });
        }
    }

    function drawSubjectReadSubjects(subject) {
        let varHtml = "";
        varHtml += "<li>";
        varHtml += "<div class='wrapper-collapsible-header'><div class='collapsible-header' onclick='readTitles(" + subject.id + ");'>" + subject.name + "</div>";
        varHtml += "<div class='subject-drop'>";
        varHtml += "<a class='dropdown-trigger btn' href='#' data-target='dropdown_subject_" + subject.id + "'><span class='material-icons-outlined'>arrow_drop_down</span></a>";
        varHtml += "<ul id='dropdown_subject_" + subject.id + "' class='dropdown-content'>";
        varHtml += "<li><a href='javascript:updateSubjectReadSubjects(" + subject.id + ",\"" + subject.name + "\");'><span class='material-icons-outlined'>drive_file_rename_outline</span></a></li>";
        varHtml += "<li><a href='javascript:deleteSubjectReadSubjects(" + subject.id + ",\"" + subject.name + "\");'><span class='material-icons-outlined'>delete</span></a></li>";
        varHtml += "</ul>";
        varHtml += "</div>";
        varHtml += "</div><div id='subject_" + subject.id + "_wrapper' class='collapsible-body'></div>";
        varHtml += "</li>";
        return varHtml;
    }

    function drawSubjectsReadSubjects(subjects) {
        let varHtml = "";
        for (let i = 0; i < subjects.length; i++) {
            varHtml += drawSubjectReadSubjects(subjects[i]);
        }
        document.getElementById("subject_list").innerHTML += varHtml;
        $(document).ready(function () {
            $('.collapsible').collapsible();
        });

        $('.dropdown-trigger').dropdown();
    }

    function readSubjects() {
        fetch('${pageContext.request.contextPath}/subject/read-all')
            .then(response => response.json())
            .then(data => {
                console.log(data);
                if (data.length > 0) {
                    drawSubjectsReadSubjects(data);
                }
            });
    }

    readSubjects();
</script>
</body>
</html>