<%@ page contentType="text/html" pageEncoding="UTF-8" %>

<!DOCTYPE html><html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/assessment/read-tasks-modal.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/template/custom-modal.css"/>
</head>
<body>
<div class="custom-modal unselectable" id="modal_tasks_read_tasks">
    <div class="custom-modal-content" id="custom_modal_content_read_tasks">
        <div class="custom-modal-header">
            <div class="custom-modal-header-title-left"></div>
            <div class="custom-modal-header-title" id="question_header_title_read_tasks"></div>
            <div class="custom-modal-header-title-right">
                <span class="custom-full" id="modal_full_read_tasks">&#x26F6;</span>
                <span class="custom-close" id="modal_close_read_tasks">&#10005;</span>
            </div>
        </div>
        <div class="divider"></div>
        <div class="custom-modal-body" id="modal_body_read_tasks">
            <div class="question-header-read-tasks" id="question_header_read_tasks"></div>
            <div class="question-body-read-tasks" id="question_body_read_tasks"></div>
            <div class="question-bottom-read-tasks" id="question_bottom_read_tasks"></div>
        </div>
        <div class="divider"></div>
        <div class="custom-modal-footer" id="modal_footer_read_tasks"></div>
    </div>
</div>
<script src="${pageContext.request.contextPath}/js/custom/screen.js"></script>
<script>
    let modal = document.getElementById("modal_tasks_read_tasks");
    let content = document.getElementById("custom_modal_content_read_tasks");
    let body = document.getElementById("modal_body_read_tasks");
    let questionBody = document.getElementById("question_body_read_tasks");
    let close = document.getElementById("modal_close_read_tasks");
    let full = document.getElementById("modal_full_read_tasks");
    let fullScreen = false;

    close.onclick = function () {
        modal.style.display = "none";
        if (fullScreen) {
            content.style.margin = "10% auto";
            content.style.width = "90%";
            content.style.height = "min-content";
            content.style.borderRadius = "3px";
            body.style.height = "min-content";
            questionBody.style.height = "min-content";
            fullScreen = false;
            closeFullscreen();
        }
    }

    full.onclick = function () {
        if (fullScreen) {
            content.style.margin = "10% auto";
            content.style.width = "90%";
            content.style.height = "min-content";
            content.style.borderRadius = "3px";
            body.style.height = "min-content";
            questionBody.style.height = "min-content";
            fullScreen = false;
            closeFullscreen();
        } else {
            content.style.margin = "auto";
            content.style.width = "100%";
            content.style.height = "100%";
            content.style.borderRadius = "0";
            body.style.height = "100%";
            questionBody.style.height = "100%";
            fullScreen = true;
            openFullscreen();
        }
    }

    function disableButtonsReadTasks(mcqTasksLength, mcqTasksCurrentIndex) {
        document.getElementById("btn_back_read_tasks").style.background = "#222c31";
        document.getElementById("btn_back_read_tasks").style.color = "#fff";
        document.getElementById("btn_back_read_tasks").style.cursor = "pointer";
        document.getElementById("btn_next_read_tasks").style.background = "#222c31";
        document.getElementById("btn_next_read_tasks").style.color = "#fff";
        document.getElementById("btn_next_read_tasks").style.cursor = "pointer";
        if (mcqTasksCurrentIndex === 1) {
            document.getElementById("btn_back_read_tasks").style.background = "#ddd";
            document.getElementById("btn_back_read_tasks").style.color = "#222c31";
            document.getElementById("btn_back_read_tasks").style.cursor = "default";
        }
        if (mcqTasksCurrentIndex === mcqTasksLength) {
            document.getElementById("btn_next_read_tasks").style.background = "#ddd";
            document.getElementById("btn_next_read_tasks").style.color = "#222c31";
            document.getElementById("btn_next_read_tasks").style.cursor = "default";
        }
    }

    function nextReadTasks(variantId) {
        let tasksLength = Number.parseInt(localStorage.getItem("tasksLength_" + variantId));
        let tasksCurrentIndex = Number.parseInt(localStorage.getItem("tasksCurrentIndex_" + variantId));
        if (tasksCurrentIndex < tasksLength) {
            validateQuestionReadTasks(variantId, tasksCurrentIndex);
        }
    }

    function backReadTasks(variantId) {
        let tasksCurrentIndex = Number.parseInt(localStorage.getItem("tasksCurrentIndex_" + variantId));
        if (tasksCurrentIndex > 1) {
            let i = tasksCurrentIndex - 2;
            validateQuestionReadTasks(variantId, i);
        }
    }

    function finishReadTasks(variantId) {
        let tasks = JSON.parse(localStorage.getItem("tasks_" + variantId));
        let isCompleted = true;
        let answers = "";
        for (let i = 0; i < tasks.length; i++) {
            if (tasks[i].isAnswered === '0') {
                isCompleted = false;
            } else {
                answers += tasks[i].isAnswered + "OzK";
            }
        }

        if (isCompleted && tasks.length > 0) {
            answers = answers.slice(0, -3);
            postParticipantReadTasks({
                "author": 1,
                "variantId": variantId,
                "answers": answers,
                "status": "f"
            });
        } else {
            M.toast({html: '<span>Барлық сұрақтарға жауап беріңіз</span>', classes: 'red accent-2'});
        }
    }

    function postParticipantReadTasks(participant) {
        fetch('${pageContext.request.contextPath}/participant/finish', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(participant)
        })
            .then(response => response.json())
            .then(response => {
                if (response) {
                    if (response.id && response.id > 0) {
                        M.toast({
                            html: '<span>СӘТТІ ТАПСЫРЫЛДЫ</span>',
                            displayLength: 1000,
                            classes: 'green',
                            completeCallback: () => {
                                readTasks(1, '2023-11-12 11:34:29.000000');
                            }
                        });
                    } else {
                        M.toast({
                            html: '<span>ТЕХ АҚАУ</span>',
                            classes: 'red accent-2'
                        });
                    }
                } else {
                    M.toast({
                        html: '<span>ТЕХ АҚАУ</span>',
                        classes: 'red accent-2'
                    });
                }
            })
    }

    function drawQuestionHeaderReadTasks(variantId, question) {
        let varHtml = "<div>" + question.index + "-сұрақ</div>";
        document.getElementById("question_header_read_tasks").innerHTML = varHtml;
    }

    function unselectCurrentIndexReadTasks(variantId) {
        let tasks = JSON.parse(localStorage.getItem("tasks_" + variantId));
        for (let i = 0; i < tasks.length; i++) {
            let index = i + 1;
            document.getElementById("btn_question_bottom_read_tasks_" + index).style.opacity = "1";
            document.getElementById("btn_question_bottom_read_tasks_" + index).style.border = "1px solid #ddd";
            document.getElementById("btn_question_bottom_read_tasks_" + index).style.background = "#ddd";
            document.getElementById("btn_question_bottom_read_tasks_" + index).style.color = "#222c31";
            document.getElementById("btn_question_bottom_read_tasks_" + index).style.fontWeight = "normal";
            if (tasks[i].isAnswered !== '0') {
                document.getElementById("btn_question_bottom_read_tasks_" + index).style.border = "1px solid #f7931e";
                document.getElementById("btn_question_bottom_read_tasks_" + index).style.background = "#f7931e";
                document.getElementById("btn_question_bottom_read_tasks_" + index).style.color = "#222c31";
            }
        }
    }

    function selectCurrentIndexReadTasks(variantId, index) {
        localStorage.setItem("tasksCurrentIndex_" + variantId, index);
        unselectCurrentIndexReadTasks(variantId);
        let button = document.getElementById("btn_question_bottom_read_tasks_" + index);
        button.style.borderBottom = "3px solid #222c31";
        button.style.fontWeight = "bold";
        button.style.color = "#222c31";
    }

    function drawQuestionBottomReadTasks(variantId, tasks) {
        let varHtml = "";
        for (let i = 0; i < tasks.length; i++) {

            if (tasks[0].participant.status === "c") {
                if (tasks[i].isAnswered === '0') {
                    varHtml += "<div class='btn-question-bottom-read-tasks' id='btn_question_bottom_read_tasks_" + tasks[i].index + "' onclick='validateQuestionReadTasks(" + variantId + ", " + i + ");'>" + tasks[i].index + "</div>";
                } else {
                    varHtml += "<div class='btn-question-bottom-read-tasks btn-question-bottom-answered-read-tasks' id='btn_question_bottom_read_tasks_" + tasks[i].index + "' onclick='validateQuestionReadTasks(" + variantId + ", " + i + ");'>" + tasks[i].index + "</div>";
                }
            } else {
                if (tasks[i].question.isAnswered === tasks[i].question.correctAnswer) {
                    varHtml += "<div class='btn-question-bottom-read-tasks correct' id='btn_question_bottom_read_tasks_" + tasks[i].index + "' onclick='validateQuestionReadTasks(" + variantId + ", " + i + ");'>" + tasks[i].index + "</div>";
                } else {
                    varHtml += "<div class='btn-question-bottom-read-tasks wrong' id='btn_question_bottom_read_tasks_" + tasks[i].index + "' onclick='validateQuestionReadTasks(" + variantId + ", " + i + ");'>" + tasks[i].index + "</div>";
                }
            }
        }
        return varHtml;
    }

    function drawModalFooterReadTasks(participant, variantId) {
        let varHtml = "";
        let percentage = Math.round(participant.totalCorrectQuestion * 100 / participant.totalQuestion);
        varHtml += "<a class='btn-back-read-tasks' href='javascript: backReadTasks(" + variantId + ");' id='btn_back_read_tasks'><span class='material-icons-outlined'>arrow_back_ios</span>Артқа</a>";
        if (participant.status === "c") {
            varHtml += "<a class='btn-finish-read-tasks' href='javascript: finishReadTasks(" + variantId + ");' id='btn_finish_read_tasks'><span class='material-icons-outlined'>sports_score</span>Аяқтау</a>";
        } else {
            varHtml += "<a class='btn-finish-read-tasks' href='javascript: showDiagramReadTasks(" + JSON.stringify(participant) + ");' id='btn_finish_read_tasks'><span class='material-icons-outlined'>query_stats</span>Анализ (" + percentage + "%)</a>";
        }
        varHtml += "<a class='btn-next-read-tasks' href='javascript: nextReadTasks(" + variantId + ");' id='btn_next_read_tasks'>Келесі<span class='material-icons-outlined'>arrow_forward_ios</span></a>";
        return varHtml;
    }
</script>
</body>
</html>