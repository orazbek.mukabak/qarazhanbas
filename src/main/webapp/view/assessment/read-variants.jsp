<%@ page contentType="text/html" pageEncoding="UTF-8" %>

<!DOCTYPE html><html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/assessment/read-variants.css"/>
</head>
<body>
<script>
    function newVariantReadVariants(id) {
        let name = document.getElementById("new_variant_name_" + id).value;
        let type = document.getElementById("new_question_type_" + id).value;
        if (name.length > 0 && name.length < 127) {
            postVariantReadVariants({"name": name, "type": type, "subjectId": id});
        } else {
            M.toast({
                html: '<span><spring:message code="signup.invalid.phone.message" /></span>',
                classes: 'red accent-2'
            });
        }
    }

    function updateVariantReadVariants(id, oldName, subjectId) {
        Swal.fire({
            variant: 'Атауын өңдеу',
            input: 'text',
            inputValue: oldName,
            inputAttributes: {autocapitalize: 'off'},
            showCancelButton: true,
            confirmButtonColor: '#222c31',
            cancelButtonColor: '#222c31',
            confirmButtonText: 'Иә',
            cancelButtonText: 'Жоқ',
            preConfirm: (name) => {

                fetch('${pageContext.request.contextPath}/variant/update', {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({"id": id, "name": name, "subjectId": subjectId})
                })
                    .then(response => {
                        M.toast({
                            html: '<span><spring:message code="signup.successful.message" /></span>',
                            displayLength: 2000,
                            classes: 'green',
                            completeCallback: () => {
                                readVariants(subjectId);
                            }
                        });
                    })

            }

        });
    }

    function deleteVariantReadVariants(id, name, fieldId) {
        Swal.fire({
            variant: '"' + name + '" нұсқаны өшіресіз бе?',
            text: "Осы нұсқаны орындаған оқушылардың нәтижелері түбегейлі жойылатын болады!",
            showCancelButton: true,
            confirmButtonColor: '#222c31',
            cancelButtonColor: '#222c31',
            confirmButtonText: 'Иә',
            cancelButtonText: 'Жоқ'
        }).then((result) => {
            if (result.value) {
                fetch('${pageContext.request.contextPath}/variant/delete/' + id, {
                    method: 'DELETE',
                })
                    .then(res => res.text())
                    .then(res => {
                            M.toast({
                                html: '<span><spring:message code="signup.successful.message" /></span>',
                                displayLength: 2000,
                                classes: 'green',
                                completeCallback: () => {
                                    readVariants(fieldId);
                                }
                            });
                        }
                    )
            }
        });
    }

    function postVariantReadVariants(variant) {
        fetch('${pageContext.request.contextPath}/variant/create?subjectId=' + variant.subjectId, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(variant)
        })
            .then(response => response.json())
            .then(response => {
                if (response) {
                    if (response.id && response.id > 0) {
                        M.toast({
                            html: '<span><spring:message code="signup.successful.message" /></span>',
                            displayLength: 2000,
                            classes: 'green',
                            completeCallback: () => {
                                readVariants(response.subjectId);
                            }
                        });
                    } else {
                        M.toast({
                            html: '<span><spring:message code="signup.tech.support.message" /></span>',
                            classes: 'red accent-2'
                        });
                    }
                } else {
                    M.toast({
                        html: '<span><spring:message code="signup.tech.support.message" /></span>',
                        classes: 'red accent-2'
                    });
                }
            })
    }

    function drawVariantReadVariants(variant) {
        let varHtml = "";
        varHtml += "<li class='collection-item'>";
        varHtml += "<div class='variant-text-wrapper-read-subjects' onclick='readTasks(" + variant.id + ", \"" + variant.updatedDate + "\");'>";
        varHtml += "<div class='variant-text-read-subjects'>" + variant.name + "</div>";
        varHtml += "</div>";
        varHtml += "<div class='variant-drop'>";
        varHtml += "<a class='dropdown-trigger btn' href='#' data-target='dropdown_variant_" + variant.id + "'><span class='material-icons-outlined'>arrow_drop_down</span></a>";
        varHtml += "<ul id='dropdown_variant_" + variant.id + "' class='dropdown-content'>";
        varHtml += "<li><a href='javascript:updateVariantReadVariants(" + variant.id + ",\"" + variant.name + "\", " + variant.fieldId + ");'><span class='material-icons-outlined'>drive_file_rename_outline</span></a></li>";
        varHtml += "<li><a href='javascript:deleteVariantReadVariants(" + variant.id + ",\"" + variant.name + "\", " + variant.fieldId + ");'><span class='material-icons-outlined'>delete</span></a></li>";
        varHtml += "</ul>";
        varHtml += "</div>";
        varHtml += "</li>";
        return varHtml;
    }

    function drawVariantsReadVariants(variants, fieldId) {
        let varHtml = "<div class='collection'>";
        for (let i = 0; i < variants.length; i++) {
            varHtml += drawVariantReadVariants(variants[i]);
        }
        varHtml += "</div>";
        varHtml += "<div class='new-variant'>";
        varHtml += "<input id='new_variant_name_" + fieldId + "' type='text' placeholder='Жаңа нұсқа'>";
        varHtml += "<button onclick='newVariantReadVariants(" + fieldId + ");'><span class='material-icons-outlined'>add</span></button>";
        varHtml += "</div>";
        document.getElementById("field_" + fieldId + "_wrapper").innerHTML = varHtml;
        $('.dropdown-trigger').dropdown();
    }

    function readVariants(fieldId) {
        fetch('${pageContext.request.contextPath}/variant/read-all-by-fieldid/' + fieldId)
            .then(response => response.json())
            .then(data => {
                drawVariantsReadVariants(data, fieldId);
            });
    }
</script>
</body>
</html>