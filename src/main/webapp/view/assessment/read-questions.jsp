<%@ page contentType="text/html" pageEncoding="UTF-8" %>

<!DOCTYPE html><html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/assessment/read-questions.css"/>
</head>
<body>
<script>
    function checkForQuestionTypeReadSubjects(question, index) {
            return drawQuestionMCQ(question, index);
    }

    function drawQuestionsMCQReadSubjects(questions, titleId) {
        let varHtml = "";
        varHtml += "<a class='btn' href='javascript:addQuestionMCQ(" + titleId + ");'>Жаңа сұрақ салу</a>";
        for (let i = 0; i < questions.length; i++) {
            varHtml += checkForQuestionTypeReadSubjects(questions[i], i + 1);
        }
        document.getElementById("right_wrapper_read_subjects").innerHTML = varHtml;
    }

    function readQuestions(titleId) {
        fetch('${pageContext.request.contextPath}/question/read-all-by-titleid/' + titleId)
            .then(response => response.json())
            .then(data => {
                drawQuestionsMCQReadSubjects(data, titleId);
            });
    }
</script>
</body>
</html>