<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html><html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/assessment/add-question-mcq.css"/>
</head>
<body>
<script>
    function addQuestionMCQ(titleId) {
        let varHtml = "";
        varHtml += "<div class='add-question-mcq-wrapper'>";
        varHtml += "<div class='add-question-mcq-text-wrapper'>";
        varHtml += "<textarea id='addQuestionMCQText' placeholder='Сұрақтың мәтіні'></textarea>";
        varHtml += "</div>";
        varHtml += "<div>";
        varHtml += "<textarea id='addQuestionMCQAnswerA' placeholder='A жауабы'></textarea>";
        varHtml += "</div>";
        varHtml += "<div>";
        varHtml += "<textarea id='addQuestionMCQAnswerB' placeholder='B жауабы'></textarea>";
        varHtml += "</div>";
        varHtml += "<div>";
        varHtml += "<textarea id='addQuestionMCQAnswerC' placeholder='C жауабы'></textarea>";
        varHtml += "</div>";
        varHtml += "<div>";
        varHtml += "<textarea id='addQuestionMCQAnswerD' placeholder='D жауабы'></textarea>";
        varHtml += "</div>";
        varHtml += "<div>";
        varHtml += "<select id='addQuestionMCQCorrectAnswer'>";
        varHtml += "<option value='-1' disabled selected>Дұрыс жауабы</option>";
        varHtml += "<option value='a'>A</option>";
        varHtml += "<option value='b'>B</option>";
        varHtml += "<option value='c'>C</option>";
        varHtml += "<option value='d'>D</option>";
        varHtml += "</select>";
        varHtml += "</div>";
        varHtml += "<div class='btn-wrapper-add-question-mcq' >";
        varHtml += "<a class='btn' href='javascript:validateAddQuestionMCQ(" + titleId + ");'>Сақтау</a>";
        varHtml += "</div>";
        varHtml += "</div>";
        document.getElementById("right_wrapper_read_subjects").innerHTML = varHtml;
    }

    function validateAddQuestionMCQ(titleId) {
        let text = document.getElementById('addQuestionMCQText').value;
        let answerA = document.getElementById('addQuestionMCQAnswerA').value;
        let answerB = document.getElementById('addQuestionMCQAnswerB').value;
        let answerC = document.getElementById('addQuestionMCQAnswerC').value;
        let answerD = document.getElementById('addQuestionMCQAnswerD').value;
        let correctAnswer = document.getElementById('addQuestionMCQCorrectAnswer').value;
        if (text !== "") {
            if (correctAnswer !== "-1") {
                postAddQuestionMCQ({
                    "text": text,
                    "answerA": answerA,
                    "answerB": answerB,
                    "answerC": answerC,
                    "answerD": answerD,
                    "correctAnswer": correctAnswer
                }, titleId);
            } else {
                M.toast({
                    html: '<span>Дұрыс жауабын көрсетіңіз!</span>',
                    classes: 'red accent-2'
                });
            }
        } else {
            M.toast({
                html: '<span>Сұрақтың мәтінін жазыңыз!</span>',
                classes: 'red accent-2'
            });
        }
    }

    function postAddQuestionMCQ(mcq, titleId) {
        fetch('${pageContext.request.contextPath}/question/create-mcq?titleId=' + titleId, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(mcq)
        })
            .then(response => response.json())
            .then(response => {
                if (response) {
                    if (response.id && response.id > 0) {
                        M.toast({
                            html: '<span>Сұрақ сәтті салынды</span>',
                            displayLength: 500,
                            classes: 'green',
                            completeCallback: () => {
                                readQuestions(titleId);
                            }
                        });
                    } else {
                        M.toast({
                            html: '<span>Қателік орын алды</span>',
                            classes: 'red accent-2'
                        });
                    }
                } else {
                    M.toast({
                        html: '<span>Қателік орын алды</span>',
                        classes: 'red accent-2'
                    });
                }
            })
    }
</script>
</body>
</html>