<%@ page contentType="text/html" pageEncoding="UTF-8" %>

<!DOCTYPE html><html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/website/banner.css" />
    <title></title>
</head>
<body>
<div class="carousel carousel-slider center" id="wrapper_read_banners">
</div>
<script>
    function drawBannerReadBanners(banner) {
        let varHtml = "";
        varHtml += "<div class=\"carousel-item white-text\" style=\"background-image: url('${pageContext.request.contextPath}/images/website/" + banner.imageUrl + "');\" href=\"#one!" + banner.id +"\">";
        varHtml += "<div class='carousel-shadow'>";
        varHtml += "<h2>" + banner.title + "</h2>";
        varHtml += "<p class='white-text'>" + banner.description + "</p>";
        varHtml += "<div class='carousel-fixed-item center'>";
        varHtml += "<a class='btn-banner' href='" + banner.clickUrl + "'>" + banner.clickName + "</a>";
        varHtml += "</div>";
        varHtml += "</div>";
        varHtml += "</div>";
        return varHtml;
    }
    function drawBannersReadBanners(banners) {
        let varHtml = "";
        for(let i = 0; i < banners.length; i++) {
            varHtml += drawBannerReadBanners(banners[i]);
        }
        document.getElementById("wrapper_read_banners").innerHTML += varHtml;

        var instance = M.Carousel.init({
            fullWidth: true,
            indicators: true
        });

        $('.carousel.carousel-slider').carousel({
            fullWidth: true,
            indicators: true
        });

        setTimeout(autoplay, 30000);
        function autoplay() {
            $('.carousel-slider').carousel('next');
            setTimeout(autoplay, 30000);
        }
    }

    function readBannersByUrlReadBanners(url) {
        fetch('${pageContext.request.contextPath}/banner/read-all-by-url/' + url)
            .then(response => response.json())
            .then(data => {
                if(data.length > 0) {
                    drawBannersReadBanners(data);
                }
            });
    }
    readBannersByUrlReadBanners(getEnvCurrentWebsiteUrl());
</script>
</body>
</html>