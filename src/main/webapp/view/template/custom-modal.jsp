<div class="custom-modal" id="modal_tasks_read_tasks">
    <div class="custom-modal-content" id="custom_modal_content_read_tasks">
        <div class="custom-modal-header">
            <div class="custom-modal-header-title-left"></div>
            <div class="custom-modal-header-title">HEADER</div>
            <div class="custom-modal-header-title-right">
                <span class="custom-full" id="modal_full_read_tasks">&#x26F6;</span>
                <span class="custom-close" id="modal_close_read_tasks">&#10005;</span>
            </div>
        </div>
        <div class="divider"></div>
        <div class="custom-modal-body" id="modal_body_read_tasks">
            BODY
        </div>
        <div class="divider"></div>
        <div class="custom-modal-footer">
            FOOTER
        </div>
    </div>
</div>
<script src="${pageContext.request.contextPath}/js/custom/screen.js"></script>
<script>
    let modal = document.getElementById("modal_tasks_read_tasks");
    let content = document.getElementById("custom_modal_content_read_tasks");
    let body = document.getElementById("modal_body_read_tasks");
    let close = document.getElementById("modal_close_read_tasks");
    let full = document.getElementById("modal_full_read_tasks");
    let fullScreen = false;

    close.onclick = function () {
        modal.style.display = "none";
        if (fullScreen) {
            content.style.margin = "10% auto";
            content.style.width = "90%";
            content.style.height = "min-content";
            content.style.borderRadius = "3px";
            body.style.height = "min-content";
            fullScreen = false;
            closeFullscreen();
        }
    }

    full.onclick = function () {
        if (fullScreen) {
            content.style.margin = "10% auto";
            content.style.width = "90%";
            content.style.height = "min-content";
            content.style.borderRadius = "3px";
            body.style.height = "min-content";
            fullScreen = false;
            closeFullscreen();
        } else {
            content.style.margin = "auto";
            content.style.width = "100%";
            content.style.height = "100%";
            content.style.borderRadius = "0";
            body.style.height = "100%";
            fullScreen = true;
            openFullscreen();
        }
    }
</script>