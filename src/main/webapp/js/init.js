// localStorage.setItem("envCurrentUserEmail", "");
// localStorage.setItem("envCurrentUserInnerId", "");
localStorage.setItem("envCurrentUploadPath", "https://firebasestorage.googleapis.com/v0/b/ozatonline/o/uploads");
localStorage.setItem("envCurrentWebsiteUrl", window.location.hostname);
if (getEnvCurrentWebsiteUrl() === "localhost") {
    localStorage.setItem("envCurrentWebsiteUrl", "texer.me");
}
if (localStorage.lastEnteredDate) {
    lastEnteredDate = Date.parse(localStorage.lastEnteredDate);
    if ((new Date() - lastEnteredDate) > 60000) {
        initEnvironemnt();
    }
} else {
    initEnvironemnt();
}
initEnvironemnt();

function getEnvCurrentUploadPath() {
    return localStorage.getItem("envCurrentUploadPath");
}

function getEnvCurrentWebsiteUrl() {
    return localStorage.getItem("envCurrentWebsiteUrl");
}

function setEnvCurrentWebsiteUrl(item) {
    localStorage.setItem("envCurrentWebsiteUrl", item);
}

function getEnvCurrentWebsiteOwnerId() {
    return localStorage.getItem("envCurrentWebsiteOwnerId");
}

function setEnvCurrentWebsiteOwnerId(item) {
    localStorage.setItem("envCurrentWebsiteOwnerId", item);
}

function getEnvCurrentWebsiteTitle() {
    return localStorage.getItem("envCurrentWebsiteTitle");
}

function setEnvCurrentWebsiteTitle(item) {
    localStorage.setItem("envCurrentWebsiteTitle", item);
}

function getEnvCurrentWebsiteDescription() {
    return localStorage.getItem("envCurrentWebsiteDescription");
}

function setEnvCurrentWebsiteDescription(item) {
    localStorage.setItem("envCurrentWebsiteDescription", item);
}

function getEnvCurrentWebsiteAddress() {
    return localStorage.getItem("envCurrentWebsiteAddress");
}

function setEnvCurrentWebsiteAddress(item) {
    localStorage.setItem("envCurrentWebsiteAddress", item);
}

function getEnvCurrentWebsiteCRMName() {
    return localStorage.getItem("envCurrentWebsiteCRMName");
}

function setEnvCurrentWebsiteCRMName(item) {
    localStorage.setItem("envCurrentWebsiteCRMName", item);
}

function getEnvCurrentWebsiteCRMMobile() {
    return localStorage.getItem("envCurrentWebsiteCRMMobile");
}

function setEnvCurrentWebsiteCRMMobile(item) {
    localStorage.setItem("envCurrentWebsiteCRMMobile", item);
}

function getEnvCurrentWebsiteCRMWhatsapp() {
    return localStorage.getItem("envCurrentWebsiteCRMWhatsapp");
}

function setEnvCurrentWebsiteCRMWhatsapp(item) {
    localStorage.setItem("envCurrentWebsiteCRMWhatsapp", item);
}

function getEnvCurrentWebsiteCRMTelegram() {
    return localStorage.getItem("envCurrentWebsiteCRMTelegram");
}

function setEnvCurrentWebsiteCRMTelegram(item) {
    localStorage.setItem("envCurrentWebsiteCRMTelegram", item);
}

function getEnvCurrentWebsiteCRMInstagram() {
    return localStorage.getItem("envCurrentWebsiteCRMInstagram");
}

function setEnvCurrentWebsiteCRMInstagram(item) {
    localStorage.setItem("envCurrentWebsiteCRMInstagram", item);
}

function getEnvCurrentWebsiteCRMEmail() {
    return localStorage.getItem("envCurrentWebsiteCRMEmail");
}

function setEnvCurrentWebsiteCRMEmail(item) {
    localStorage.setItem("envCurrentWebsiteCRMEmail", item);
}

function getEnvCurrentWebsiteTechSupportMobileNumber() {
    return localStorage.getItem("envCurrentWebsiteTechSupportMobileNumber");
}

function setEnvCurrentWebsiteTechSupportMobileNumber(item) {
    localStorage.setItem("envCurrentWebsiteTechSupportMobileNumber", item);
}

function initEnvironemnt() {
    fetch("/chain/read/" + getEnvCurrentWebsiteUrl() + "")
        .then(response => response.json()
            .then(data => {
                if (data.ownerId && data.ownerId > 0) {
                    setEnvCurrentWebsiteOwnerId(data.ownerId);
                    setEnvCurrentWebsiteTitle(data.title);
                    setEnvCurrentWebsiteDescription(data.description);
                    setEnvCurrentWebsiteAddress(data.address);
                    setEnvCurrentWebsiteCRMName(data.crmname);
                    setEnvCurrentWebsiteCRMMobile(data.crmmobile);
                    setEnvCurrentWebsiteCRMWhatsapp(data.crmwhatsapp);
                    setEnvCurrentWebsiteCRMTelegram(data.crmtelegram);
                    setEnvCurrentWebsiteCRMInstagram(data.crminstagram);
                    setEnvCurrentWebsiteCRMEmail(data.crmemail);
                    setEnvCurrentWebsiteTechSupportMobileNumber(data.techSupportMobileNumber);
                    localStorage.lastEnteredDate = new Date();
                }
            })
        );
}