function displayTechSupportHeader() {
    Swal.fire({
        type: 'question',
        title: getEnvCurrentWebsiteTechSupportMobileNumber(),
        text: getEnvCurrentWebsiteCRMEmail(),
        confirmButtonColor: '#222c31',
        confirmButtonText: 'OK'
    });
}

function youtubeIdParser(url) {
    url = url.split(/(vi\/|v=|\/v\/|youtu\.be\/|\/embed\/)/);
    return (url[2] !== undefined) ? url[2].split(/[^0-9a-z_\-]/i)[0] : url[0];
}

window.onload = function() {
    (function($){
        $(function(){
            $('.sidenav').sidenav();
        });
    })(jQuery);
//    $('.navbar-nav>li>a').on('click', function(){
//        $('.navbar-collapse').collapse('hide');
//    });
//    $(document).ready(function () {
//        $(document).click(function (event) {
//            var click = $(event.target);
//            var _open = $(".navbar-collapse").hasClass("show");
//            if(_open === true && !click.hasClass("navbar-toggler")) {
//                $(".navbar-toggler").click();
//            }
//        });
//    });
};