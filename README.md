<img src="./src/main/webapp/images/git/banner.jpg" >

#  "KARAZHANBASMUNAI" JSC
## INSTALL & UPDATE UBUNTU
Install Ubuntu 22.04LTS to the server.
```shell
sudo apt update
```
```shell
sudo apt upgrade
```
```shell
sudo reboot
```
## INSTALL JDK
```shell
sudo apt install openjdk-17-jdk -y
```
```shell
sudo apt install openjdk-17-jre
```
```shell
java --version
```
```shell
cd /usr/lib/jvm
```
```shell
ls
```
```shell
cd ..
```
```shell
sudo nano /etc/environment
```
If not running then give permission to the folder.
```shell
sudo chmod a+rwx /etc/environment
```
```shell
JAVA_HOME="/usr/lib/jvm/java-17-openjdk-amd64"
```
```shell
cd ..
```
```shell
source /etc/environment
```
```shell
echo $JAVA_HOME
```
## INSTALL MAVEN
```shell
sudo apt install maven -y
```
## INSTALL NGINX
```shell


```
```shell
sudo ufw allow 'Nginx Full'
```
```shell
systemctl status nginx
```
## INSTALL GIT
```shell
sudo apt install git
```
## DEPLOPYMENT
```shell
sudo git clone https://gitlab.com/orazbek.mukabak/qarazhanbas.git
```
```shell
cd qarazhanbas
```
```shell
sudo mvn clean package
```
```shell
./mvnw spring-boot:run
```
If not running sudo chmod a+rwx mvnw
```shell
lsof -i :8080
```
```shell
kill -9 63262
```
```shell
sh startup.sh
```
```shell
sh restart.sh
```
```shell
sh shutdown.sh
```
```shell
tail -f log.txt
```
## INSTALL MYSQL
```shell
sudo apt install mysql-server -y
```
```shell
sudo mysql
```
```shell
alter user 'root'@'localhost' identified with mysql_native_password by 'Ozatsite!';
```
```shell
exit;
```
```shell
mysql_secure_installation
```
y 0 n y y y y
```shell
mysql -u root -p
```
```shell
create database ozatsite_userinfo;
create database ozatsite_messenger;
create database ozatsite_website;
create database ozatsite_assessment;
create database ozatsite_employment;
```
```shell
create user 'ozatsite'@'localhost' identified with mysql_native_password by 'Ozatsite!';
```
```shell
use mysql;
```
```shell
select user from user;
```
```shell
grant all on ozatsite_userinfo.* to 'ozatsite'@'localhost';
grant all on ozatsite_messenger.* to 'ozatsite'@'localhost';
grant all on ozatsite_website.* to 'ozatsite'@'localhost';
grant all on ozatsite_assessment.* to 'ozatsite'@'localhost';
grant all on ozatsite_employment.* to 'ozatsite'@'localhost';
```
```shell
exit;
```
## INSTALL PHPMYADMIN
```shell
sudo apt install phpmyadmin php-mbstring php-zip php-gd php-json php-curl
```
```shell
sudo apt-get install php8.1-fpm -y
```
```shell
sudo systemctl status php8.1-fpm
```
```shell
Do not select any web server
```
```shell
sudo ln -s /usr/share/phpmyadmin /var/www/html/phpmyadmin
```
```shell
cd /etc/nginx/sites-available/
```
```shell
sudo chmod 777 default
```
```shell
nano default
```
```shell
server {
    listen 80 default_server;
    listen [::]:80 default_server;

    location /phpmyadmin {
    index index.php;
    }

    root /var/www/html;
    index index.html index.htm index.nginx-debian.html;

#    server_name texer.me www.texer.me;
    location / {
        try_files $uri $uri/ /index.php;
    }

    location ~ ^/(doc|sql|setup)/ {
        deny all;
    }

    location ~ \.php$ {
        fastcgi_pass unix:/run/php/php8.1-fpm.sock;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        include fastcgi_params;
        include snippets/fastcgi-php.conf;
    }

    location ~ /\.ht {
        deny all;
    }
}
```
```shell
sudo systemctl restart nginx
```
```shell
http://185.4.180.127/phpmyadmin/index.php
```
## INSTALL DOMAIN
A @ 185.4.180.127 3600
```shell
sudo vi /etc/nginx/sites-available/texer.me
```
```shell
server {
    server_name texer.me;
    index index.html index.htm;
    access_log /var/log/nginx/texer.log;
    error_log  /var/log/nginx/texer-error.log error;
    location / {
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header Host $http_host;
        proxy_pass http://127.0.0.1:8080;
        proxy_redirect off;
    }
}
```
```shell
:wq
```
```shell
sudo ln -s /etc/nginx/sites-available/texer.me /etc/nginx/sites-enabled/texer.me
```
```shell
sudo nginx -t
```
```shell
sudo nginx -s reload
```

## INSTALL SSL
```shell
sudo apt install snapd
```
```shell
sudo snap install --classic certbot
```
```shell
sudo certbot --nginx -d texer.me
```